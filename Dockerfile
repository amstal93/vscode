# docker run -d \
#  -v /tmp/.X11-unix:/tmp/.X11-unix \
#  -v $HOME:/home/tom \
#  -e DISPLAY=unix$DISPLAY \
#  --device /dev/dri \
#  --name vscode \
#  -- hostname vscode \
#  letompouce/vscode

FROM debian:bullseye-slim
LABEL maintainer "ToM <tom@leloop.org>"

ENV HOME /home/tom
RUN useradd --create-home --home-dir $HOME tom \
	&& chown -R tom:tom $HOME \
	&& groupadd --system -g 999 docker \
	&& usermod -aG docker tom

COPY start.sh /usr/local/bin/start.sh

WORKDIR $HOME
CMD [ "start.sh" ]


# Tell debconf to run in non-interactive mode
ENV DEBIAN_FRONTEND noninteractive

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
# hadolint ignore=DL3008,DL3009
RUN apt-get --quiet --quiet update \
    && apt-get install -y --no-install-recommends --quiet --quiet \
        apt-transport-https \
        bash-completion \
        ca-certificates \
        curl \
        git \
        gnupg \
        openssh-client \
        shellcheck \
    && curl -sSL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor | apt-key add - \
    && echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list \
    && apt-get --quiet --quiet update \
    && apt-get -y --quiet --quiet --no-install-recommends install \
        code \
        libasound2 \
        libatk1.0-0 \
        libcairo2 \
        libcups2 \
        libexpat1 \
        libfontconfig1 \
        libfreetype6 \
        libgtk2.0-0 \
        libpango-1.0-0 \
        libx11-xcb1 \
        libxcb-dri3-0 \
        libxcomposite1 \
        libxcursor1 \
        libxdamage1 \
        libxext6 \
        libxfixes3 \
        libxi6 \
        libxrandr2 \
        libxrender1 \
        libxss1 \
        libxtst6 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
